#include <iostream>
using namespace std;
#include <fstream>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>

#include "service.h"

#define	SIZE 	100
#define	MILLI	1000

class Application {
	private:
		int 	CSExecTime;
		int		delayBetweenTwoCSRequests;
		int 	nCSReq;
		
	public:
		Application ();
		Application (Service);
		int getCSExecTime ();
		int getDelayBetweenTwoCSRequests ();
		int getNumberOfCSRequests ();
};

Application::Application () {
	CSExecTime 					= 	-1;
	delayBetweenTwoCSRequests 	= 	-1;
	nCSReq 						= 	-1;
}

Application::Application (Service svc) {
	CSExecTime = svc.getCSExecTime ();
	delayBetweenTwoCSRequests = svc.getDelayBetweenTwoCSRequests ();
	nCSReq = svc.getNumberOfCSRequests ();
}

int	Application::getCSExecTime () {
	return CSExecTime;
}
int	Application::getDelayBetweenTwoCSRequests () {
	return delayBetweenTwoCSRequests;
}
int	Application::getNumberOfCSRequests () {
	return nCSReq;
}

int main (int argc, char * argv []) {
	Service 		svc;
	Application 	app;
	int				i;
	ofstream		ofile;
	char 			temp [SIZE];
	
	// Server Variables
	int			thID;
	pthread_t	server;
	
	if (argc < 2) {
		cout << "Usage: " << argv[0] << " <NODE ID> [<config file name>]" << endl;
		return 1;
	}
	else if (argc == 2) {
		svc = Service (atoi (argv[1]));
	}
	else if (argc == 3) {
		// Second Argument being the configuration file
		svc = Service (atoi (argv[1]), argv[2]);
	}
	else {
		cout << "Usage: " << argv[0] << " <NODE ID> [<config file name>]" << endl;
		return 1;
	}
	
	app = Application (svc);
	
	cout << "Node " << svc.getNodeID () << " -- Starting Server.." << endl;
	svc.log ("Starting Server");
	
	thID = pthread_create (&server, NULL, &Service::serverThread, (void *) &svc);
	if (thID) {
		cout << "Node " << svc.getNodeID () << " -- Error Creating a thread. Dying..." << endl;
		svc.log ("Error Creating a thread. Dying...");
		svc.errorLog ("Error Creating a thread. Dying...");
		exit (1);
	}
	sleep (1);
	cout << "Node " << svc.getNodeID () << " -- " << "Application Starting.." << endl;
	svc.log ("Applicaton Starting..");
	
	cout << "Node " << svc.getNodeID () << " -- " << "CS Execution Time: " << app.getCSExecTime () << endl;
	cout << "Node " << svc.getNodeID () << " -- " << "Delay between CS Requests: " << app.getDelayBetweenTwoCSRequests () << endl;
	cout << "Node " << svc.getNodeID () << " -- " << "No. of CS Requests: " << app.getNumberOfCSRequests () << endl;
	
	for (i = 0; i < app.getNumberOfCSRequests (); i++) {
		// Request for CS
		svc.cs_enter ();
		
		// Once Control Returns, Enter CS. In this case, write to file;
		ofile.open ("CS", ios::app);
		
		ofile << svc.getNodeID () << ": Entering CS at " << svc.getClock () << endl; //To put Time Stamp
		usleep (app.getCSExecTime () * MILLI);
		ofile << svc.getNodeID () << ": Exiting CS at " << svc.getClock () << endl;
		
		ofile.close ();
		
		// Announce termination of CS
		svc.cs_leave ();
		
		// Delay between two consecutive Requests
		usleep (app.getDelayBetweenTwoCSRequests () * MILLI);
	}
	
	cout << "Node " << svc.getNodeID () << " -- " << "Successful implementation. Terminating.." << endl;
	svc.log ("Successful implementation. Terminating..");
	
	// cout << "Node " << svc.getNodeID () << " -- " << "Queue Status: " << svc.QEmpty () << endl;
	// sprintf (temp, "Queue Status: %d", svc.QEmpty ());
	// svc.log (temp);
	
	// for (i = 0; i < 15; i++) {
		// svc.cs_leave ();
		// sleep (1);
	// }
	// while (true);
	
	sleep (15);
	return 0;
}
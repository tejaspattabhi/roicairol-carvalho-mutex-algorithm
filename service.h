#ifndef SERVICE_H
#define SERVICE_H

#include <iostream>
using namespace std;
#include <fstream>
#include <queue>

#define	SIZE 			100
#define CS_REQ_RAISED	2
#define EXECUTING_CS	1
#define IDLE			0

#define PRESENT			1
#define ABSENT 			0

class Clock {
	private:
		int cValue;
	
	public:
		Clock ();
		
		void 	updateClock (int);
		void 	updateClock ();
		int 	getClock ();
};

class Key {
	private:
		int nodeOne;
		int nodeTwo;
		bool iHave;
		
	public:
		Key ();
		Key (int, int);
		Key (int, int, bool);
		
		int 	getNodeOne ();
		int 	getNodeTwo ();
		bool 	doIHave ();
		void 	IHave ();
		void 	releaseKey ();
};

class Node {
	private:
		int 	nodeID;
		char 	hostname [SIZE];
		int 	port;
	
	public:
		Node ();
		Node (int);
		Node (int, char *, int);
		
		int 	getNodeID ();
		void 	getHostName (char *);
		int 	getPort ();
};

class Service {
	private:
		int 		nodeID;
		int 		nodeCount;
		int 		keyCount; // Count of the number of keys I currently have.
		Key 		myKeys [SIZE];
		Node 		nodes [SIZE];
		Clock 		clk;
		int			status;
			
		char 		configFile [SIZE];
			
		int 		CSExecTime;
		int			delayBetweenTwoCSRequests;
		int 		nCSReq;
		queue<int> 	reqQueue;
		
		
		void 	generateKeys ();
		void 	fetchKeys ();
		bool 	isAvailable (Key);
		void 	keyTaken (Key);
		
	public:
		Service ();
		Service (int, char * = NULL);
		
		void 	cs_enter ();
		void 	cs_leave ();
		
		Node	getNode (int);
		Node *	getNodes ();
		int 	getNodeCount ();
		int		getNodeID ();
		int 	getCSExecTime ();
		int		getDelayBetweenTwoCSRequests ();
		int 	getNumberOfCSRequests ();
		
		void 	updateClock (int);
		void 	updateClock ();
		int 	getClock ();
		
		static	void * 	serverThread (void *);
		
		Key	*	getKey (int, int);
		int		doIHaveAllKeys ();
		int		getNodeWhichHasKey (Key);
		
		void 	pack (char *, int);
		int		getStatus ();
		
		void 	QInsert (int);
		int		QDelete ();
		bool 	QEmpty ();
		
		void 	log (const char *);
		void	errorLog (const char *);
};

#endif
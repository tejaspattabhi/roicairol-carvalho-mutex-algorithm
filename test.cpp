#include <iostream>
using namespace std;
#include <fstream>

#include "service.h"

#define	ERRORS		1
#define	NO_ERRORS	0

bool is_empty (ifstream& pFile) {
    return pFile.peek() == std::ifstream::traits_type::eof();
}

int main (int argc, char * argv []) {
	Service 	svc;
	Node *		nodes;
	int			nCount, i, nID;
	char		fName [SIZE];
	int			fd;
	ifstream	ifile;
	
	int			termination;
	
	if (argc != 2) {
		cout << "Usage: " << argv[0] << " CONFIG_FILE_NAME" << endl;
		return 1;
	}
	
	nodes 		= 	NULL;
	nCount 		= 	0;
	nID 		= 	-1;
	termination	=	NO_ERRORS;
	
	// Fake node ID 0
	svc = Service (0, argv [1]);
	nodes = svc.getNodes ();
	nCount = svc.getNodeCount ();
	
	for (i = 0; i < nCount; i++) {
		nID = nodes[i].getNodeID ();
		
		sprintf (fName, "error_%d", nID);
		ifile.open (fName);
		
		if (!is_empty (ifile)) {
			cout << "Errors found in the node " << nID << endl;
			termination = ERRORS;
		}
		
		ifile.close ();
	}

	if (termination == NO_ERRORS) {
		cout << "Mutex implemented Successfully" << endl;
	}
	
	return 0;
}

AOS Project 2 - Roucairol & Carvalho MutEx Algorithm
==
Project Coded in C++
--

#### Please follow the steps for execution:


**Step 1**: To compile, log on to `CS1/CS2.utdallas.edu` machines ONLY. This is 
		because the DC Machines DO NOT HAVE TCP Libraries to be linked for C/C++.

**Step 2**: Create your own configuration file in the format:

	# A line prefixed with '#' is treated as a comment
	# But the order in which the information is placed matters
	# First the total Number of Nodes
	<An Integer>
	# Then the list of Node IDs, it's hostname and port number
    <NODE ID1>	<HOSTNAME>	<PORT>	
    <NODE ID2>	<HOSTNAME> 	<PORT>	
    ..

	# Total number of requests for CS per node
	<An Integer>

	# Average delay between two consecutive Critical Section Requests for each Node
	<An Integer - treated as milliseconds>

	# Average time taken for the execution of the Critical Section 
	<An Integer - treated as milliseconds>
    
Save the filename as "config" or some custom name.

**Step 3**: If a custom file (name which is not "config") is created, edit 
		"initialize.sh", and change the "configFile" to your custom name.

**Step 4**: Edit the "**initialize.sh**" for few more parameters:

- User Executing the code should replace his/her NetID against the value of "remoteuser" for successful 
   execution. (Unless it is the Super User.)

- MAKE SURE THAT THE (NODE ID, HOSTNAME)s LISTED ON THE CONFIG FILE
   ARE LISTED AS SAME (VALUE AND ORDER) IN THE "remotecomputer" variables in the initialize.sh.
		   
**Step 5**: Execution:

Run the command:

    > cd *location_for_all_the_files_listed_in_Note_2_below*
	> g++ service.cpp application.cpp -o node -pthread
    > sh initialize.sh
    	   
**Step 6**: Output -- The Output can be seen on screen (mixed with all node processes)
		or can also be viewed in a file for each node.
		Output file Names is after the <NODE ID>
		Example, if the node ID is "1", the output file created is also "1"
		By, "cat 1", you can view the output logs. In case of errors, each node has an independent error file named like: "error\__nodeid_". That can be viewed too.

For testing, you need to compile the following code and run.
		
		> g++ service.cpp test.cpp -o test
		> ./test <config_file_name>

The testing output can be seen on the screen.
		
#################################################################################
Note:

1. If you want to compile the code manually (note with the initialize.sh script)
   then,

    	> g++ service.cpp application.cpp -o node -pthread
   
2. Make sure that the following files are in the same folder for successful
   execution
   - `initialize.sh`
   - `service.cpp`
   - `service.h`
   - `application.cpp`
   - `config` "or" `custom_knowledge_graph_file`
   - `test.cpp`
#################################################################################

*Please contact me on any kind of concerns/questions*

**By,** 
### Tejas Tovinkere Pattabhi 
**Email: tejas.pattabhi@gmail.com**

**Net ID: txp130630**
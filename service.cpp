#include <iostream>
using namespace std;
#include <fstream>
#include <queue>

#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>

#include "service.h"

#define	SIZE 			100
#define CS_REQ_RAISED	2
#define EXECUTING_CS	1
#define IDLE			0

#define PRESENT			1
#define ABSENT 			0

#define INCREMENTED		1
#define NOT_INCREMENTED	0

#define REQUEST_KEY		1
#define	KEY				0

#define SOCKET_ERROR 	-1
#define QUEUE_SIZE		10

/* Functions and method description of class Clock */
Clock::Clock () {
	// Initialize Logical Clock
	cValue = 0;
}

void Clock::updateClock () {
		cValue += 1;
}

void Clock::updateClock (int mClockValue) {
	if (mClockValue > cValue) 
		cValue = mClockValue + 1;
	else
		cValue += 1;
}

int Clock::getClock () {
	return cValue;
}

/* Functions and method description of class Key */

Key::Key () {
	nodeOne = nodeTwo = -1;
	iHave = false;
}

Key::Key (int nOne, int nTwo) {
	if (nOne < nTwo) {
		nodeOne = nOne;
		nodeTwo = nTwo;
	}
	else {
		nodeOne = nTwo;
		nodeTwo = nOne;
	}
	// nOne will never be equal to nTwo
	iHave = false;
}

Key::Key (int nOne, int nTwo, bool iHave) {
	if (nOne < nTwo) {
		nodeOne = nOne;
		nodeTwo = nTwo;
	}
	else {
		nodeOne = nTwo;
		nodeTwo = nOne;
	}
	this->iHave = iHave;
}

int Key::getNodeOne () {
	return nodeOne;
}

int Key::getNodeTwo () {
	return nodeTwo;
}

bool Key::doIHave () {
	return iHave;
}

void Key::IHave () {
	iHave = true;
}

void Key::releaseKey () {
	iHave = false;
}

/* Functions and method description of class Node */
Node::Node () {
	nodeID = -1;
	strcpy (hostname, "");
	port = -1;
}

Node::Node (int nID) {
	nodeID = nID;
	strcpy (hostname, "");
	port = -1;
}

Node::Node (int nID, char * hName, int pNum) {
	nodeID = nID;
	strcpy (hostname, hName);
	port = pNum;
}

int Node::getNodeID () {
	return nodeID;
}

void Node::getHostName (char * buffer) {
	strcpy (buffer, hostname);
}

int Node::getPort () {
	return port;
}

/* Functions and method description of class Service */

Service::Service () {
	nodeID = nodeCount = keyCount = -1;
	status = IDLE;
	strcpy (configFile, "config");
}

Service::Service (int nodeID, char * config) {
	// open config file and read the Node Information
	// generate the keys that this node is having.
	// some more parameters to be read.
	// Trigger the server for hosting the response.
	
	ifstream 	ifile;
	char 		buffer [SIZE * 5], temp [SIZE], value [SIZE];
	int 		i;
	
	// Flush log files
	ofstream 	ofile;
	
	// Server Variables
	int			thID;
	pthread_t	server;
	
	if (config != NULL)
		strcpy (configFile, config);
	else
		strcpy (configFile,	"config");
	
	this->nodeID = nodeID;
	ifile.open (configFile);
	
	// Flush the previous log files
	sprintf (temp, "%d", nodeID);
	ofile.open (temp);
	ofile.close ();
	
	sprintf (temp, "error_%d", nodeID);
	ofile.open (temp);
	ofile.close ();
	
	
	// Read Config File
	ifile >> buffer;
	if (ifile.eof ()) {
		cout << "Node " << getNodeID () << " -- config file error" << endl;
		log ("config file error");
		errorLog ("config file error");
		exit (1);
	}
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			cout << "Node " << getNodeID () << " -- config file error" << endl;
			log ("config file error");
			errorLog ("config file error");
			exit (1);
		}		
	}
	
	nodeCount = atoi (buffer);
	
	ifile >> buffer;
	if (ifile.eof ()) {
		cout << "Node " << getNodeID () << " -- config file error" << endl;
		log ("config file error");
		errorLog ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			cout << "Node " << getNodeID () << " -- config file error" << endl;
			log ("config file error");
			errorLog ("config file error");
			exit (1);
		}		
	}
	
	ifile >> temp >> value;
	if (ifile.eof ()) {
		cout << "Node " << getNodeID () << " -- config file error" << endl;
		log ("config file error");
		errorLog ("config file error");
		exit (1);
	}
	
	nodes[0] = Node (atoi (buffer), temp, atoi (value));
	for (i = 1; i < nodeCount; i++) {
		ifile >> buffer >> temp >> value;
		if (ifile.eof ()) {
			cout << "Node " << getNodeID () << " -- config file error" << endl;
			log ("config file error");
			errorLog ("config file error");
			exit (1);
		}		
		nodes[i] = Node (atoi (buffer), temp, atoi (value));
	}
	
	ifile >> buffer;
	if (ifile.eof ()) {
		cout << "Node " << getNodeID () << " -- config file error" << endl;
		log ("config file error");
		errorLog ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			cout << "Node " << getNodeID () << " -- config file error" << endl;
			exit (1);
		}		
	}
	
	// Confusion, where to read the remaining 3 parameters. Technically it should be in the application section. Not here.
	nCSReq = atoi (buffer);
	
	ifile >> buffer;
	if (ifile.eof ()) {
		cout << "Node " << getNodeID () << " -- config file error" << endl;
		log ("config file error");
		errorLog ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			cout << "Node " << getNodeID () << " -- config file error" << endl;
			log ("config file error");
			errorLog ("config file error");
			exit (1);
		}		
	}
	
	delayBetweenTwoCSRequests = atoi (buffer);
	
	ifile >> buffer;
	if (ifile.eof ()) {
		cout << "Node " << getNodeID () << " -- config file error" << endl;
		log ("config file error");
		errorLog ("config file error");
		exit (1);
	}
	
	// Omit Comments
	while (buffer [0] == '#') {
		ifile.getline (buffer, SIZE * 5);
		ifile >> buffer;
		if (ifile.eof ()) {
			cout << "Node " << getNodeID () << " -- config file error" << endl;
			log ("config file error");
			errorLog ("config file error");
			exit (1);
		}		
	}
	
	CSExecTime = atoi (buffer);
	
	ifile.close ();
	
	generateKeys ();
	fetchKeys ();
	
	status = IDLE;
	
	// Start server for listening
	// thID = pthread_create (&server, NULL, &Service::serverThread, (void *) this);
	// if (thID) {
		// cout << "Node " << getNodeID () << " -- \nError Creating a thread. Dying...\n" << endl;
		// exit (1);
	// }
}

void Service::updateClock () {
	clk.updateClock ();
}

void Service::updateClock (int mClockValue) {
	clk.updateClock (mClockValue);
}

int Service::getClock () {
	return clk.getClock ();
}

bool Service::isAvailable (Key k) {
	ifstream ifile;
	char temp [SIZE], buffer [SIZE];
	int nOne, nTwo;
	
	ifile.open ("keys");
	
	while (!ifile.eof ()) {
		ifile >> temp >> buffer;
		
		if (atoi (temp) == k.getNodeOne () && atoi (buffer) == k.getNodeTwo ()) {
			ifile.close ();
			return true;
		}
	}
	
	ifile.close ();
	return false;
}

int Service::getNodeID () {
	return nodeID;
}

Node * Service::getNodes () {
	return nodes;
}

int Service::getNodeCount () {
	return nodeCount;
}

void Service::keyTaken (Key k) {
	ofstream ofile;
	
	ofile.open("keys", ios::app);
	ofile << k.getNodeOne () << "\t" << k.getNodeTwo () << endl;
	ofile.close ();
}

void Service::generateKeys () {
	int i, j;
	
	j = 0;
	for (i = 0; i < nodeCount; i++) {
		if (i != nodeID) {
			myKeys [j++] = Key (nodeID, i);
		}
	}
	
	// cout << "Node " << getNodeID () << " -- Verification of Key Generation:" << endl;
	// for (i = 0; i < nodeCount - 1; i++) {
		// cout << "Node " << getNodeID () << " -- Key " << (i+1) << ": (" << myKeys [i].getNodeOne () << ", " << myKeys [i].getNodeTwo () << ")\n";
	// }
}

void Service::fetchKeys () {
	int i;
	
	for (i = 0; i < nodeCount - 1; i++) {
		if (!isAvailable (myKeys[i])) {
			myKeys[i].IHave ();
			
			// cout << "Node " << getNodeID () << " -- Taken Key " << (i+1) << ": (" << myKeys [i].getNodeOne () << ", " << myKeys [i].getNodeTwo () << ")\n";
			keyTaken (myKeys[i]);
		}
	}
}

int Service::getNodeWhichHasKey (Key k) {
	int i;
	
	if (k.getNodeOne () != nodeID)
		return k.getNodeOne ();
	else
		return k.getNodeTwo ();
}

Key * Service::getKey (int n1, int n2) {
	int i;
	
	for (i = 0; i < nodeCount - 1; i++) {
		if ((myKeys[i].getNodeOne () == n1 && myKeys[i].getNodeTwo () == n2) || (myKeys[i].getNodeOne () == n2 && myKeys[i].getNodeTwo () == n1))
			return &myKeys[i];
	}
}

int	Service::doIHaveAllKeys () {
	int i;
	
	for (i = 0; i < nodeCount - 1; i++) {
		if (!myKeys[i].doIHave ())
			return ABSENT;
	}
	return PRESENT;
}

int	Service::getCSExecTime () {
	return CSExecTime;
}

int	Service::getDelayBetweenTwoCSRequests () {
	return delayBetweenTwoCSRequests;
}

int	Service::getNumberOfCSRequests () {
	return nCSReq;
}

void Service::pack (char * buffer, int msgName) {
	memset (buffer, 0, SIZE);
			
	if (msgName == REQUEST_KEY)
		sprintf (buffer, "REQUEST_KEY|%d|%d#", nodeID, clk.getClock ());
	else
		sprintf (buffer, "KEY|%d|%d#", nodeID, clk.getClock ());
}

void Service::QInsert (int nID) {
	reqQueue.push (nID);
}

int Service::QDelete () {
	int temp;
	
	temp = reqQueue.front ();
	reqQueue.pop ();
	
	return temp;
}

bool Service::QEmpty () {
	return reqQueue.empty ();
}

Node Service::getNode (int nID) {
	int i;
	
	for (i = 0; i < nodeCount; i++) {
		if (nodes[i].getNodeID () == nID)
			return nodes[i];
	}
	
	return Node ();
}

int	Service::getStatus () {
	return status;
}

void Service::cs_enter () {
	// Invoked by application when the CS request is generated
	// Returns only when the application can enter CS
	int		sndEvent;
	int		i;
	int 	nID;
	char 	msg [SIZE];
	Node	node;
	char 	temp [SIZE];
	
	// TCP Variables
	int 	hSocket;
    struct 	hostent * 		pHostInfo;
    struct 	sockaddr_in 	Address;
    long 	nHostAddress;
	char 	strHostName[SIZE];
	
	status 	= 	CS_REQ_RAISED;
	node 	=	-1;
	strcpy (msg, "");
	
	sndEvent = NOT_INCREMENTED;
	
	for (i = 0; i < nodeCount - 1; i++) {
		if (!myKeys[i].doIHave ()) {
			cout << "Node " << getNodeID () << " -- " << "Clock: " << clk.getClock () << endl;
			sprintf (temp, "Clock: %d", clk.getClock ());
			log (temp);
			
			cout << "Node " << getNodeID () << " -- " << "I dont have the key (" << myKeys[i].getNodeOne () << ", " << myKeys[i].getNodeTwo () << ")" << endl;
			sprintf (temp, "I dont have the key (%d, %d)", myKeys[i].getNodeOne (), myKeys[i].getNodeTwo ());
			log (temp);
			
			nID = getNodeWhichHasKey (myKeys[i]);
			
			cout << "Node " << getNodeID () << " -- " << "The Key is with Node " << nID << endl;
			sprintf (temp, "The Key is with Node %d", nID);
			log (temp);
			
			//Check for Clock Increment
			if (sndEvent != INCREMENTED) {
				// Send Event (Request Multicast)
				clk.updateClock ();
				cout << "Node " << getNodeID () << " -- " << "Clock: " << clk.getClock () << endl;
				sprintf (temp, "Clock: %d", clk.getClock ());
				log (temp);
				
				sndEvent = INCREMENTED;
			}
			
			pack (msg, REQUEST_KEY);
			node = getNode (nID);
			
			// Establish TCP connection
			// Send Message to "node"
			
			hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (hSocket == SOCKET_ERROR)	{
				cout << "Node " << getNodeID () << " -- CS Enter: Could not make a socket\n";
				log ("CS Enter: Could not make a socket");
				errorLog ("CS Enter: Could not make a socket");
				exit (1);
			}
			
			node.getHostName (strHostName);
			cout << "Node " << getNodeID () << " -- CS Enter: Connecting to Host: " << strHostName << "\tPort: " << node.getPort () << endl;
			sprintf (temp, "CS Enter: Connecting to Host %s, Port %d", strHostName, node.getPort ());
			log (temp);
			
			pHostInfo = gethostbyname (strHostName);
			memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

			Address.sin_addr.s_addr = 	nHostAddress;
			Address.sin_port 		= 	htons (node.getPort ());
			Address.sin_family 		= 	AF_INET;	
			
			if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
				cout << "Node " << getNodeID () << " -- CS Enter: Could not connect to host " << node.getNodeID () << " (" << strHostName << ")\n";
				sprintf (temp, "CS Enter: Could not connect to host %d, %s, Port %d", node.getNodeID (), strHostName, node.getPort ());
				log (temp);
				errorLog (temp);
			
				perror ("Error");
				exit (1);
			}

			cout << "Node " << getNodeID () << " -- CS Enter: Sending message to " << node.getNodeID () << ": " << msg << endl;
			sprintf (temp, "CS Enter: Sending message to %d: %s", node.getNodeID (), msg);
			log (temp);
			
			write (hSocket, msg, strlen (msg));
			
			if (close (hSocket) == SOCKET_ERROR)	{
				cout << "Node " << getNodeID () << " -- CS Enter: Could not close socket\n";
				log ("CS Enter: Could not close socket");
				errorLog ("CS Enter: Could not close socket");
				exit (1);
			}
		}
	}
	
	log ("Waiting for all Keys");
	while (doIHaveAllKeys () != PRESENT);
	
	// I have all keys, now give GO for CS execution
	status 	= 	EXECUTING_CS;
	cout << "Node " << getNodeID () << " -- " << "Received all Keys, entering CS" << endl;
	log ("Received all Keys, entering CS");

	cout << "Node " << getNodeID () << " -- " << "Clock: " << clk.getClock () << endl;
	sprintf (temp, "Clock: %d", clk.getClock ());
	log (temp);
}

void Service::cs_leave () {
	// Executed only when the CS is completed by the application. Releases keys incase of any requests made by other processes.
	Node	node;
	int 	nID;
	Key *	key;
	char	msg [SIZE];
	char	temp [SIZE];
	
	// TCP Variables
	int 	hSocket;
    struct 	hostent * 		pHostInfo;
    struct 	sockaddr_in 	Address;
    long 	nHostAddress;
	char 	strHostName[SIZE];

	key		= 	NULL;
	nID		= 	-1;
	strcpy (msg, "");
	
	
	cout << "Node " << getNodeID () << " -- " << "Entering cs_leave ()" << endl;
	log ("Entering cs_leave ()");
	
	cout << "Node " << getNodeID () << " -- " << "Clock: " << clk.getClock () << endl;
	sprintf (temp, "Clock: %d", clk.getClock ());
	log (temp);
	
	cout << "Node " << getNodeID () << " -- " << "Is Queue Empty? " << QEmpty () << endl;
	sprintf (temp, "Is Queue Empty? %d", QEmpty ());
	log (temp);
	
	
	while (!QEmpty ()) {
	    nID = QDelete ();
		node = getNode (nID);
		
		key = getKey (nID, nodeID);
		key->releaseKey ();
		
		// Send Event
		clk.updateClock ();
		cout << "Node " << getNodeID () << " -- " << "Clock: " << clk.getClock () << endl;
		sprintf (temp, "Clock: %d", clk.getClock ());
		log (temp);
	
		pack (msg, KEY);
		cout << "Node " << getNodeID () << " -- " << "Establishing a TCP connection to connect to " << nID << endl;
		sprintf (temp, "Establishing a TCP connection to connect to %d", nID);
		log (temp);
	
		// Establish TCP Connection with node, send "msg" as KEY
		hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (hSocket == SOCKET_ERROR)	{
			cout << "Node " << getNodeID () << " -- CS Leave: Could not make a socket\n";
			log ("CS Leave: Could not make a socket");
			errorLog ("CS Leave: Could not make a socket");
			exit (1);
		}
		
		node.getHostName (strHostName);
		cout << "Node " << getNodeID () << " -- CS Leave: Connecting to Host: " << strHostName << "\tPort: " << node.getPort () << endl;
		sprintf (temp, "CS Leave: Connecting to Host %s, Port %d", strHostName, node.getPort ());
		
		pHostInfo = gethostbyname (strHostName);
		memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

		Address.sin_addr.s_addr = 	nHostAddress;
		Address.sin_port 		= 	htons (node.getPort ());
		Address.sin_family 		= 	AF_INET;	
		
		if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
			cout << "Node " << getNodeID () << " -- CS Leave: Could not connect to host " << node.getNodeID () << " (" << strHostName << ")\n";
			sprintf (temp, "CS Leave: Could not connect to host %d, %s", node.getNodeID (), strHostName);
			exit (1);
		}
		
		cout << "Node " << getNodeID () << " -- CS Leave: Sending message to " << node.getNodeID () << ": " << msg << endl;
		sprintf (temp, "CS Leave: Sending message to %d: %s", node.getNodeID (), msg);
		log (temp);
					
		write (hSocket, msg, strlen (msg));
		
		// Close TCP Connection
		if (close (hSocket) == SOCKET_ERROR)	{
			cout << "Node " << getNodeID () << " -- CS Leave: Could not close socket\n";
			log ("CS Leave: Could not close a socket");
			errorLog ("CS Leave: Could not close a socket");
			exit (1);
		}
	}
	
	status 	= 	IDLE;
}

// Thread which responds to other service (key) requests
void * 	Service::serverThread (void * ptr) {
	Service * 	svc;
	char 		msg [SIZE];
	int 		nID, mClkVal;
	Key *		key;
	Node 		node;
	
	char 		buffer [SIZE];
	char 		temp [SIZE];
	char 		msgs [SIZE / 10][SIZE];
	int			msgCount, i;
	
	// TCP Variables
	int 		hSocket, hServerSocket;
	struct 		sockaddr_in 	sAddress;
	int 		nAddressSize;
	
	struct 		hostent * 		pHostInfo;
    struct 		sockaddr_in 	Address;
    long 		nHostAddress;
	char 		strHostName[SIZE];
	char *		str;
	
	strcpy (msg, "");
	nAddressSize 	= 	sizeof (struct sockaddr_in);
	svc 			= 	(Service *) ptr;
	nID 			= 	-1;
	mClkVal			= 	-1;
	key 			= 	NULL;
	str 			= 	NULL;
	msgCount		= 	0;
	i				= 	0;
	
	
	hServerSocket = socket (AF_INET, SOCK_STREAM, 0);
	if (hServerSocket == SOCKET_ERROR)	{
		cout << "Node " << svc->getNodeID () << " -- Server: Could not make a socket\n";
		svc->log ("Server: Could not make a socket");
		svc->errorLog ("Server: Could not make a socket");
		exit (1);
	}

	(svc->getNode (svc->getNodeID ())).getHostName (buffer);
	cout << "Node " << svc->getNodeID () << " -- " << "My HostName: " << buffer << "\tMy Port: " << (svc->getNode (svc->getNodeID ())).getPort () << endl;
	sprintf (temp, "My HostName is %s, Port %d", buffer, (svc->getNode (svc->getNodeID ())).getPort ());
	svc->log (temp);
	
	memset (&sAddress, '0', sizeof (sAddress));
	bzero ((void *) &sAddress, sizeof (sAddress));
	sAddress.sin_addr.s_addr 	= 	htonl (INADDR_ANY);
	sAddress.sin_port 			= 	htons ((svc->getNode (svc->getNodeID ())).getPort ());
	sAddress.sin_family 		= 	AF_INET;

	if (bind (hServerSocket, (struct sockaddr *) &sAddress, sizeof (sAddress)) == SOCKET_ERROR)	{
		cout << "Node " << svc->getNodeID () << " -- Server: Could not bind\n";
		svc->log ("Server: Could not bind");
		svc->errorLog ("Server: Could not bind");
		close (hServerSocket);
		// perror ("Bind");
		exit (1);
	}

	getsockname (hServerSocket, (struct sockaddr *) &sAddress, (socklen_t *) &nAddressSize);
	if (listen (hServerSocket, QUEUE_SIZE) == SOCKET_ERROR)    {
		cout << "Node " << svc->getNodeID () << " -- Server: Could not listen\n";
		svc->log ("Server: Could not listen");
		svc->errorLog ("Server: Could not listen");
		close (hServerSocket);
		exit (0);
	}
	
	cout << "Node " << svc->getNodeID () << " -- " << "Server: Ready for listening" << endl;
	svc->log ("Server: Ready for listening");
		
	while (true) {
		// Read Message to msg
		hSocket = accept (hServerSocket, (struct sockaddr*) &sAddress, (socklen_t *) &nAddressSize);
		cout << "Node " << svc->getNodeID () << " -- " << "Server: Accepted a connection" << endl;
		svc->log ("Server: Accepted a connection");
		
		memset (msg, 0, SIZE);
		read (hSocket, msg, SIZE);
		
		cout << "Node " << svc->getNodeID () << " -- " << "Server: Message Received: " << msg << endl;
		sprintf (temp, "Server: Message Received - %s", msg);
		svc->log (temp);
		
		if (close (hSocket) == SOCKET_ERROR)	{
			cout << "Node " << svc->getNodeID () << " -- Server: Location 1 - Could not close socket\n";
			svc->log ("Server: Location 1 - Could not close socket");
			svc->errorLog ("Server: Location 1 - Could not close socket");
			close (hServerSocket);
			exit (1);
		}
		
		msgCount = 0;
		str = strtok (msg, "#");
		while (str != NULL) {
			memset (msgs[msgCount], 0, SIZE);
			strcpy (msgs[msgCount], str);
			msgCount++;
			str = strtok (NULL, "#");
		}
		
		str = NULL;
		
		for (i = 0; i < msgCount; i++) {
			cout << "Node " << svc->getNodeID () << " -- " << "Server: Processing Message: " << msgs[i] << endl;
			sprintf (temp, "Server: Processing Message %s", msgs[i]);
			svc->log (temp);
		
			str = strtok (msgs[i], "|");
			strcpy (buffer, str);
			
			str = strtok (NULL, "|");
			nID = atoi (str);
			
			str = strtok (NULL, "#");
			mClkVal = atoi (str);
			
			// sscanf (msg, "%s|%d|%d#", buffer, &nID, &mClkVal);
			
			cout << "Node " << svc->getNodeID () << " -- " << buffer << ", " << nID << ", " << mClkVal << endl;
			sprintf (temp, "Server: %s, %d, %d", buffer, nID, mClkVal);
			svc->log (temp);
		
		
			if (!strcmp (buffer, "KEY")) {
				// status should be CS_REQ_RAISED
				
				// Receive Event
				svc->updateClock (mClkVal);
				cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
				sprintf (temp, "Clock: %d", svc->getClock ());
				svc->log (temp);
				
				key = svc->getKey (nID, svc->getNodeID ());
				
				// Receive Key updation
				key->IHave ();
				cout << "Node " << svc->getNodeID () << " -- " << "Do i have the key (" << key->getNodeOne () << ", " << key->getNodeTwo () << ")?" << key->doIHave() << endl;
				sprintf (temp, "Do i have the key (%d, %d)? %d", key->getNodeOne (), key->getNodeTwo (), key->doIHave());
				svc->log (temp);
			}
			else {
				// Three cases of statuses: IDLE | EXECUTING_CS | CS_REQ_RAISED
				
				cout << "Node " << svc->getNodeID () << " -- " << "The message received is a RELEASE_KEY request" << endl;
				svc->log ("The message received is a REQUEST_KEY request");
				
				// Case 1: When IDLE, simply give away the key
				if (svc->getStatus () == IDLE) {
					cout << "Node " << svc->getNodeID () << " -- " << "Server: I am IDLE"<< endl;
					svc->log ("Server: I am IDLE");
					
					// Receive Event
					svc->updateClock (mClkVal);
					cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
					sprintf (temp, "Clock: %d", svc->getClock ());
					svc->log (temp);
					
					key = svc->getKey (nID, svc->getNodeID ());
					key->releaseKey ();
					cout << "Node " << svc->getNodeID () << " -- " << "Do i have the key (" << key->getNodeOne () << ", " << key->getNodeTwo () << ")?" << key->doIHave() << endl;
					sprintf (temp, "Do i have the key (%d, %d)? %d", key->getNodeOne (), key->getNodeTwo (), key->doIHave());
					svc->log (temp);
					
					// Send Event
					svc->updateClock ();
					cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
					sprintf (temp, "Clock: %d", svc->getClock ());
					svc->log (temp);
					svc->pack (msg, KEY);
					node = svc->getNode (nID);
					
					// Establish TCP connection and Send Message
					hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
					if (hSocket == SOCKET_ERROR)	{
						cout << "Node " << svc->getNodeID () << " -- Server: Location 2 - Could not make a socket\n";
						svc->log ("Server: Location 2 - Could not close socket");
						svc->errorLog ("Server: Location 2 - Could not close socket");
						close (hServerSocket);
						exit (1);
					}
					
					node.getHostName (strHostName);
					cout << "Node " << svc->getNodeID () << " -- Server: Connecting to Host: " << strHostName << "\tPort: " << node.getPort () << endl;
					sprintf (temp, "Server: Connecting to Host %s, Port %d", strHostName, node.getPort ());
					svc->log (temp);
					
					pHostInfo = gethostbyname (strHostName);
					memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

					Address.sin_addr.s_addr = 	nHostAddress;
					Address.sin_port 		= 	htons (node.getPort ());
					Address.sin_family 		= 	AF_INET;	
					
					if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
						cout << "Node " << svc->getNodeID () << " -- Server: Could not connect to host " << node.getNodeID () << " (" << strHostName << ")\n";
						sprintf (temp, "Server: Could not connect to host %d (%s)", node.getNodeID (), strHostName);
						svc->log (temp);
					
						close (hServerSocket);
						exit (1);
					}
					
					cout << "Node " << svc->getNodeID () << " -- Server: Sending message to " << node.getNodeID () << ": " << msg << endl;
					sprintf (temp, "Server: Sending message to %d: %s", node.getNodeID (), msg);
					svc->log (temp);
			
					write (hSocket, msg, strlen (msg));
					
					// Close TCP Connection
					if (close (hSocket) == SOCKET_ERROR)	{
						cout << "Node " << svc->getNodeID () << " -- Server: Location 3 - Could not close socket\n";
						svc->log ("Server: Location 3 - Could not close socket");
						svc->errorLog ("Server: Location 3 - Could not close socket");
						
						close (hServerSocket);
						exit (1);
					}
					
				}
				else if (svc->getStatus () == EXECUTING_CS) { // Case 2: EXECUTING_CS, Just buffer the requests in a queue
					// Receive Event
					cout << "Node " << svc->getNodeID () << " -- " << "EXECUTIUNG_CS is my status, inserting into Queue"<< endl;
					svc->log ("EXECUTIUNG_CS is my status, inserting into Queue");
					
					svc->updateClock (mClkVal);
					cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
					sprintf (temp, "Clock: %d", svc->getClock ());
					svc->log (temp);
					
					svc->QInsert (nID);
					cout << "Node " << svc->getNodeID () << " -- " << "(" << nID << ", " << mClkVal << ") in Queue" << endl;
					sprintf (temp, "(%d, %d)", nID, mClkVal);
					svc->log (temp);
					
					// Will be released during cs_leave ()
				}
				else { // Case 3: When the current node has a CS Request: CS_REQ_RAISED
					// Three Cases:
					if (mClkVal > svc->getClock ()) {
						// Receive Event
						svc->updateClock (mClkVal);
						cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
						sprintf (temp, "Clock: %d", svc->getClock ());
						svc->log (temp);
						
						svc->QInsert (nID);
						cout << "Node " << svc->getNodeID () << " -- " << "(" << nID << ", " << mClkVal << ") in Queue" << endl;
						sprintf (temp, "(%d, %d)", nID, mClkVal);
						svc->log (temp);
						// Will be released during cs_leave ()
					}
					else if (mClkVal == svc->getClock ()) {
						if (svc->getNodeID () < nID) {
							// Receive Event
							svc->updateClock (mClkVal);
							cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
							sprintf (temp, "Clock: %d", svc->getClock ());
							svc->log (temp);
							
							svc->QInsert (nID);
							cout << "Node " << svc->getNodeID () << " -- " << "(" << nID << ", " << mClkVal << ") in Queue" << endl;
							sprintf (temp, "(%d, %d)", nID, mClkVal);
							svc->log (temp);
							// Will be released during cs_leave ()
						}
						else {
							// Receive Event
							svc->updateClock (mClkVal);
							cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
							sprintf (temp, "Clock: %d", svc->getClock ());
							svc->log (temp);
				
							key = svc->getKey (svc->getNodeID (), nID);
							key->releaseKey ();
							
							// Send Event
							svc->updateClock ();
							cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
							sprintf (temp, "Clock: %d", svc->getClock ());
							svc->log (temp);
				
							node = svc->getNode (nID);
							svc->pack (msg, KEY);
							
							// Establish TCP connection and send msg KEY
							hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
							if (hSocket == SOCKET_ERROR)	{
								cout << "Node " << svc->getNodeID () << " -- Server: Location 4 - Could not make a socket\n";
								svc->log ("Server: Location 4 - Could not close socket");
								svc->errorLog ("Server: Location 4 - Could not close socket");
								close (hServerSocket);
								exit (1);
							}
							
							node.getHostName (strHostName);
							cout << "Node " << svc->getNodeID () << " -- Server: Connecting to Host: " << strHostName << "\tPort: " << node.getPort () << endl;
							sprintf (temp, "Server: Connecting to Host %s, Port %d", strHostName, node.getPort ());
							svc->log (temp);
					
							pHostInfo = gethostbyname (strHostName);
							memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

							Address.sin_addr.s_addr = 	nHostAddress;
							Address.sin_port 		= 	htons (node.getPort ());
							Address.sin_family 		= 	AF_INET;	
							
							if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
								cout << "Node " << svc->getNodeID () << " -- Server: Could not connect to host " << node.getNodeID () << " (" << strHostName << ")\n";
								close (hServerSocket);
								exit (1);
							}
							
							cout << "Node " << svc->getNodeID () << " -- Server: Sending message to " << node.getNodeID () << ": " << msg << endl;
							sprintf (temp, "Server: Sending message to %d: %s", node.getNodeID (), msg);
							svc->log (temp);
					
							write (hSocket, msg, strlen (msg));
							
							// Close and Open Socket
							if (close (hSocket) == SOCKET_ERROR)	{
								cout << "Node " << svc->getNodeID () << " -- Server: Location 6 - Could not close socket\n";
								svc->log ("Server: Location 6 - Could not close socket");
								svc->errorLog ("Server: Location 6 - Could not close socket");
								close (hServerSocket);
								exit (1);
							}
							
							hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
							if (hSocket == SOCKET_ERROR)	{
								cout << "Node " << svc->getNodeID () << " -- Server: Location 4 - Could not make a socket\n";
								svc->log ("Server: Location 4 - Could not close socket");
								svc->errorLog ("Server: Location 4 - Could not close socket");
								close (hServerSocket);
								exit (1);
							}
							
							node.getHostName (strHostName);
							cout << "Node " << svc->getNodeID () << " -- Server: Connecting to Host: " << strHostName << "\tPort: " << node.getPort () << endl;
							sprintf (temp, "Server: Connecting to Host %s, Port %d", strHostName, node.getPort ());
							svc->log (temp);
					
							pHostInfo = gethostbyname (strHostName);
							memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

							Address.sin_addr.s_addr = 	nHostAddress;
							Address.sin_port 		= 	htons (node.getPort ());
							Address.sin_family 		= 	AF_INET;	
							
							if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
								cout << "Node " << svc->getNodeID () << " -- Server: Could not connect to host " << node.getNodeID () << " (" << strHostName << ")\n";
								close (hServerSocket);
								exit (1);
							}
							
							// Send Event
							svc->updateClock ();
							cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
							sprintf (temp, "Clock: %d", svc->getClock ());
							svc->log (temp);
							
							svc->pack (msg, REQUEST_KEY);
							cout << "Node " << svc->getNodeID () << " -- Server: Sending message to " << node.getNodeID () << ": " << msg << endl;
							sprintf (temp, "Server: Sending message to %d: %s", node.getNodeID (), msg);
							svc->log (temp);
					
							write (hSocket, msg, strlen (msg));
							
							// Close TCP Connection
							if (close (hSocket) == SOCKET_ERROR)	{
								cout << "Node " << svc->getNodeID () << " -- Server: Location 6 - Could not close socket\n";
								svc->log ("Server: Location 6 - Could not close socket");
								svc->errorLog ("Server: Location 6 - Could not close socket");
								close (hServerSocket);
								exit (1);
							}
							// Close connection
						}
					}
					else { // mClkVal < getClock ()
						// Receive Event
						svc->updateClock (mClkVal);
						cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
						sprintf (temp, "Clock: %d", svc->getClock ());
						svc->log (temp);
				
						key = svc->getKey (svc->getNodeID (), nID);
						key->releaseKey ();
						
						// Send Event
						svc->updateClock ();
						cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
						sprintf (temp, "Clock: %d", svc->getClock ());
						svc->log (temp);
				
						node = svc->getNode (nID);
						svc->pack (msg, KEY);
						
						// Establish TCP connection and send msg KEY
						hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
						if (hSocket == SOCKET_ERROR)	{
							cout << "Node " << svc->getNodeID () << " -- Server: Could not make a socket\n";
							sprintf (temp, "Server: Could not make a socket");
							svc->log (temp);
							svc->errorLog (temp);
							
							close (hServerSocket);
							exit (1);
						}
						
						node.getHostName (strHostName);
						cout << "Node " << svc->getNodeID () << " -- Server: Connecting to Host: " << strHostName << "\tPort: " << node.getPort () << endl;
						sprintf (temp, "Server: Connecting to Host %s, Port %d", strHostName, node.getPort ());
						svc->log (temp);
					
						pHostInfo = gethostbyname (strHostName);
						memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

						Address.sin_addr.s_addr = 	nHostAddress;
						Address.sin_port 		= 	htons (node.getPort ());
						Address.sin_family 		= 	AF_INET;	
						
						if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
							cout << "Node " << svc->getNodeID () << " -- Server: Could not connect to host " << node.getNodeID () << " (" << strHostName << ")\n";
							sprintf (temp, "Server: Could not connect to host %d (%s)", node.getNodeID (), strHostName);
							svc->log (temp);
							svc->errorLog (temp);
							
							close (hServerSocket);
							exit (1);
						}
						
						// Send KEY
						cout << "Node " << svc->getNodeID () << " -- Server: Sending message to " << node.getNodeID () << ": " << msg << endl;
						sprintf (temp, "Server: Sending message to %d: %s", node.getNodeID (), msg);
						svc->log (temp);
				
						write (hSocket, msg, strlen (msg));
						
						if (close (hSocket) == SOCKET_ERROR)	{
							cout << "Node " << svc->getNodeID () << " -- Server: Location 7 - Could not close socket\n";
							svc->log ("Server: Location 7 - Could not close socket");
							svc->errorLog ("Server: Location 7 - Could not close socket");
							
							close (hServerSocket);
							exit (1);
						}
						
						node = svc->getNode (nID);
						
						// Establish TCP connection and send msg REQUEST_KEY
						hSocket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
						if (hSocket == SOCKET_ERROR)	{
							cout << "Node " << svc->getNodeID () << " -- Server: Could not make a socket\n";
							sprintf (temp, "Server: Could not make a socket");
							svc->log (temp);
							svc->errorLog (temp);
							
							close (hServerSocket);
							exit (1);
						}
						
						node.getHostName (strHostName);
						cout << "Node " << svc->getNodeID () << " -- Server: Connecting to Host: " << strHostName << "\tPort: " << node.getPort () << endl;
						sprintf (temp, "Server: Connecting to Host %s, Port %d", strHostName, node.getPort ());
						svc->log (temp);
					
						pHostInfo = gethostbyname (strHostName);
						memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);

						Address.sin_addr.s_addr = 	nHostAddress;
						Address.sin_port 		= 	htons (node.getPort ());
						Address.sin_family 		= 	AF_INET;	
						
						if (connect (hSocket, (struct sockaddr *) &Address, sizeof (Address)) == SOCKET_ERROR)	{
							cout << "Node " << svc->getNodeID () << " -- Server: Could not connect to host " << node.getNodeID () << " (" << strHostName << ")\n";
							sprintf (temp, "Server: Could not connect to host %d (%s)", node.getNodeID (), strHostName);
							svc->log (temp);
							svc->errorLog (temp);
							
							close (hServerSocket);
							exit (1);
						}
						
						// Now place a request for the same key
						// Send Event
						svc->updateClock ();
						cout << "Node " << svc->getNodeID () << " -- " << "Clock: " << svc->getClock () << endl;
						sprintf (temp, "Clock: %d", svc->getClock ());
						svc->log (temp);
						
						svc->pack (msg, REQUEST_KEY);
						
						// send msg REQUEST_KEY message
						cout << "Node " << svc->getNodeID () << " -- Server: Sending message to " << node.getNodeID () << ": " << msg << endl;
						sprintf (temp, "Server: Sending message to %d: %s", node.getNodeID (), msg);
						svc->log (temp);
					
						write (hSocket, msg, strlen (msg));
						
						// Close TCP Connection
						if (close (hSocket) == SOCKET_ERROR)	{
							cout << "Node " << svc->getNodeID () << " -- Server: Location 7 - Could not close socket\n";
							svc->log ("Server: Location 7 - Could not close socket");
							svc->errorLog ("Server: Location 7 - Could not close socket");
							
							close (hServerSocket);
							exit (1);
						}
						// Close connection
					}
				}
			}
		}
	}
} 

void Service::log (const char * str) {
	ofstream 	ofile;
	char		fName [SIZE];
	
	sprintf (fName, "%d", getNodeID ());
	
	ofile.open (fName, ios::app);
	ofile << str << endl;
	ofile.close ();
}

void Service::errorLog (const char * str) {
	ofstream 	ofile;
	char		fName [SIZE];
	
	sprintf (fName, "error_%d", getNodeID ());
	
	ofile.open (fName, ios::app);
	ofile << str << endl;
	ofile.close ();
}

